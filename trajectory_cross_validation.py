import numpy as np
import networkx as nx
import torch as pt
import pytorch_lightning as ptl
import torch_geometric as ptg
import torch
from torch.nn import Sequential as Seq, Linear, ReLU
from torch_geometric.nn import MessagePassing
from pytorch_lightning import LightningModule
from torch.optim.adam import Adam
from torch.optim import SparseAdam, Adagrad
from pytorch_lightning.loggers import TensorBoardLogger
from torch.multiprocessing import Pool,  set_start_method
from torch_geometric.loader import DataLoader, DataListLoader
from pysindy.differentiation import FiniteDifference, SmoothedFiniteDifference
#from torch.utils.data import DataLoader
import warnings
import hypernetx
import collections
import sys
import ray.tune as tune
import ray.air as air
#from ray_lightning.tune import TuneReportCallback
from ray.tune.integration.pytorch_lightning import TuneReportCallback
from ray.tune.schedulers import ASHAScheduler
from ray.tune import CLIReporter
from models import HigherOrderModel
from hypergraphs import simple_ERHypergraph_Dataset, MyData, simple_ERHypergraph_Trajectories
import dynamics
import os
warnings.filterwarnings(
    'ignore', message="An output with one or more elements was resized since it had shape", category=UserWarning)


#cross_validation function

def crossvalidate_dataset(config, data=None, data_dir=None,  order=None, non_linear=False, nonlinear_cut=0, num_epochs=100, num_workers=32, cv_fold=1, num_folds=3, hyperparameter_tuning=False):
    model = HigherOrderModel(
        config, order, non_linear=non_linear, nonlinear_cut=nonlinear_cut)

    # Cut the data set into the folds for cross validation
    lower_bound = int(cv_fold/num_folds*len(data))
    upper_bound = int((cv_fold+1)/num_folds*len(data))
    train_dataset = data[:lower_bound]
    train_dataset.extend(data[upper_bound:])
    test_dataset = data[lower_bound:upper_bound]

    model_name = "model_non_linear" + \
        str(non_linear) + '_'+str(nonlinear_cut)+'_fold'+str(cv_fold)

    if hyperparameter_tuning:
        save_dir = tune.get_trial_dir()
    else:
        save_dir = os.getcwd() + "/" + data_dir

    #data = [ptg.data.Data(x = X[:,i].reshape(-1, 1), y=Y[:,i].reshape(-1,1), edge_index=L) for i in range(X.shape[1])]
    train_loader = DataLoader(train_dataset, batch_size=20,
                              shuffle=True, pin_memory=True, num_workers=num_workers)
    val_loader = DataLoader(test_dataset, batch_size=20,
                            pin_memory=True, num_workers=num_workers)

    #early_stopping = ptl.callbacks.EarlyStopping('val_loss', stopping_threshold=0.01, patience=50)
    trainer = ptl.Trainer(accelerator='gpu', devices=1, max_epochs=num_epochs, enable_progress_bar=False, default_root_dir=save_dir+"/"+model_name, logger=TensorBoardLogger(
        save_dir=save_dir, name=model_name, version="."), callbacks=[
        TuneReportCallback(
            {
                "loss": "val_loss"
            },
            on="validation_end")
    ])
    trainer.fit(model, train_loader, val_loader)


#def hyperparameter_search(config, data=None, dataset_name=None, order=2, non_linear = False, nonlinear_cut=2, num_epochs=100, cv_fold=1, num_folds=3):
 #   crossvalidate_dataset(config, data, dataset_name, order=order, non_linear = non_linear, nonlinear_cut=nonlinear_cut, num_epochs=100, cv_fold=1, num_folds=3)


dataset_name = 'ERHypergraphXGI'
order = 4
dataset_network_size = 20
dataset_sample_size = 25
time_delta = 0.01
number_time_steps = 100
noise = False
smoothed = False
noise_strength=1
t = np.linspace(0, number_time_steps*time_delta, number_time_steps)
initialisation = "01uniform"

for nonlinear_cut_data in [2,3,4]:
    lifting_func = dynamics.SILiftingFunction_new(order, nonlinear_cut_data)
    data_generator = simple_ERHypergraph_Trajectories(
        dataset_name, dataset_network_size, order, dataset_sample_size, lifting_func, number_time_steps, time_delta, initialisation=initialisation, )
    traj_data = data_generator.data
    data = []
    for datapoint in traj_data:
        dense_L = datapoint.hyper_edge_index
        number_time_steps = len(datapoint.y)
        if smoothed:
            sfd = SmoothedFiniteDifference(smoother_kws={'window_length': 50})
        else:
            sfd = FiniteDifference()
        if noise:
            orig = torch.stack(datapoint.y).squeeze()
            trajectory = orig+time_delta * noise_strength*torch.randn(orig.size())
            diff_trajectory = sfd._differentiate(trajectory, t)
        else:
            trajectory = torch.stack(datapoint.y).squeeze()
            diff_trajectory = sfd._differentiate(trajectory, t)
        for i in range(number_time_steps-1):
            data.append(
                MyData(x=trajectory[i, :].reshape(-1, 1), y=torch.from_numpy(diff_trajectory[i, :].reshape(-1, 1)), hyper_edge_index=dense_L))
                #MyData(x=datapoint.y[i], y=(datapoint.y[i+1]-datapoint.y[i])*100, hyper_edge_index=dense_L))
    
    #Shuffle data
    np.random.shuffle(data)


    non_linear = True
    num_workers = 10
    num_epochs = 100
    num_samples = 10
    if noise == True and smoothed == False:
        data_dir = 'results/cross_validation/' + dataset_name +'/trajectories/noise/data_' + str(dataset_sample_size) + '_' + str(
            dataset_network_size) + '_' + str(order) + '_' + lifting_func.name() + '_' + initialisation + "_" + str(noise_strength)
    elif noise == True and smoothed == True:
        data_dir = 'results/cross_validation/' + dataset_name + '/trajectories/noise/smoothed/data_' + str(dataset_sample_size) + '_' + str(
            dataset_network_size) + '_' + str(order) + '_' + lifting_func.name() + '_' + initialisation + "_" + str(noise_strength)
    else:
        data_dir = 'results/cross_validation/' + dataset_name + '/trajectories/data_' + str(dataset_sample_size) + '_' + str(
            dataset_network_size) + '_' + str(order) + '_' + lifting_func.name() + '_' + initialisation

    for nonlinear_cut_model in [2,3]:

        config = {'learning_rate': tune.loguniform(1e-7, 1e-1),
                  'regularization': tune.loguniform(1e-7, 1e-1),
                  'num_layers': tune.grid_search([1, 2, 3])}

        reporter = CLIReporter(
            parameter_columns=["num_layers",
                               'learning_rate', 'regularization'],
            metric_columns=["loss"])

        scheduler = ASHAScheduler(
            max_t=num_epochs,
            grace_period=1,
            reduction_factor=2)

        train_fn_with_parameters = tune.with_parameters(crossvalidate_dataset, data=data, data_dir=data_dir, order=order, non_linear=non_linear,
                                                        nonlinear_cut=nonlinear_cut_model, num_epochs=num_epochs, num_workers=num_workers, cv_fold=1, num_folds=3, hyperparameter_tuning=True)

        resources_per_trial = {"cpu": 1, "gpu": 1/num_workers}

        tuner = tune.Tuner(
            tune.with_resources(
                train_fn_with_parameters,
                resources=resources_per_trial
            ),
            tune_config=tune.TuneConfig(
                metric="loss",
                mode="min",
                scheduler=scheduler,
                num_samples=num_samples,
            ),
            run_config=air.RunConfig(
                name=data_dir+"_model_non_linear" +
                str(non_linear) + '_'+str(nonlinear_cut_model),
                progress_reporter=reporter,
            ),
            param_space=config,
        )
        #Tuner = tune.Tuner(tune.with_parameters(crossvalidate_dataset, data=data, dataset_name=dataset_name, order=order, non_linear = non_linear, nonlinear_cut=nonlinear_cut, num_epochs=100, cv_fold=1, num_folds=3))
        analysis = tuner.fit()

        #analysis=tune.run(train_fn_with_parameters,
        #                  metric='val_loss', mode='max', config=config, local_dir='tune_hyperparameter_search', name='test', resources_per_trial={'gpu':1/num_workers}, num_samples=1, verbose=1, progress_reporter=tune.CLIReporter(print_intermediate_tables=True,metric_columns=["val_loss"]))

        print(analysis.get_best_result(metric="loss", mode="min").config)

        with Pool(num_workers) as p:
            p.starmap(crossvalidate_dataset, [(analysis.get_best_result(metric="loss", mode="min").config, data,
                      data_dir, order, non_linear, nonlinear_cut_model, num_epochs, 0, i, 10, False) for i in range(10)])
