import numpy as np
import networkx as nx
import torch as pt
import pytorch_lightning as ptl
import torch_geometric as ptg
import torch
import xgi
from torch.nn import Sequential as Seq, Linear, ReLU
from torch_geometric.nn import MessagePassing
from pytorch_lightning import LightningModule
from torch.optim.adam import Adam
from torch.optim import SparseAdam, Adagrad
from pytorch_lightning.loggers import TensorBoardLogger
from torch.multiprocessing import Pool,  set_start_method
from torch_geometric.loader import DataLoader, DataListLoader
#from torch.utils.data import DataLoader
import warnings
import hypernetx
import collections
import sys
import ray.tune as tune
import ray.air as air
#from ray_lightning.tune import TuneReportCallback
from ray.tune.integration.pytorch_lightning import TuneReportCallback
from ray.tune.schedulers import ASHAScheduler
from ray.tune import CLIReporter
from models import HigherOrderModel
from hypergraphs import Hypergraph_Generator, HashFunction
import dynamics
import os
warnings.filterwarnings('ignore', message="An output with one or more elements was resized since it had shape", category=UserWarning)



#cross_validation function

def crossvalidate_dataset(config, data=None, data_dir=None,  order=None, non_linear=False, nonlinear_cut=0, num_epochs = 100,num_workers=32, cv_fold=1, num_folds=3, hyperparameter_tuning=False):
    model = HigherOrderModel(config,order,non_linear=non_linear, nonlinear_cut=nonlinear_cut)

    # Cut the data set into the folds for cross validation
    lower_bound = int(cv_fold/num_folds*len(data)) 
    upper_bound = int((cv_fold+1)/num_folds*len(data))
    train_dataset = data[:lower_bound]
    train_dataset.extend(data[upper_bound:])
    test_dataset = data[lower_bound:upper_bound]

    model_name = "model_non_linear"+str(non_linear)+ '_'+str(nonlinear_cut)+'_fold'+str(cv_fold)

    if hyperparameter_tuning:
        save_dir=tune.get_trial_dir()
    else:
        save_dir=os.getcwd() + "/" + data_dir 


    #data = [ptg.data.Data(x = X[:,i].reshape(-1, 1), y=Y[:,i].reshape(-1,1), edge_index=L) for i in range(X.shape[1])]
    train_loader = DataLoader(train_dataset, batch_size=10, shuffle=True, pin_memory=True, num_workers=num_workers)
    val_loader = DataLoader(test_dataset, batch_size=10, pin_memory=True, num_workers=num_workers)

    #early_stopping = ptl.callbacks.EarlyStopping('val_loss', stopping_threshold=0.01, patience=50)
    trainer = ptl.Trainer(accelerator='gpu', devices=1, max_epochs=num_epochs, enable_progress_bar=False, default_root_dir=save_dir+"/"+model_name, logger=TensorBoardLogger(
            save_dir=save_dir, name=model_name, version="."), callbacks=[
            TuneReportCallback(
                {
                    "loss": "val_loss"
                },
                on="validation_end")
        ])
    trainer.fit(model, train_loader, val_loader)


#def hyperparameter_search(config, data=None, dataset_name=None, order=2, non_linear = False, nonlinear_cut=2, num_epochs=100, cv_fold=1, num_folds=3):
 #   crossvalidate_dataset(config, data, dataset_name, order=order, non_linear = non_linear, nonlinear_cut=nonlinear_cut, num_epochs=100, cv_fold=1, num_folds=3)


dataset_name = 'SocioPatterns_highschool'
H = xgi.read_edgelist(
    "data/real_data/contact-high-school/hyperedges-contact-high-school.txt", delimiter=",")
h_dict = xgi.to_hyperedge_dict(H)
order = 4
edges_raw = [[*h_dict[i]] for i in h_dict.keys() if len(h_dict[i]) >
         1 and len(h_dict[i]) <= order]
h = HashFunction()
edges = []
for edge in edges_raw:
    edges.append([h.apply(x) for x in edge])
dataset_network_size = len(H.nodes)
dataset_sample_size = 100
initialisation = "01uniform"
for nonlinear_cut_data in [2,3,4]:
    lifting_func=dynamics.SILiftingFunction(order, nonlinear_cut_data)
    data_generator = Hypergraph_Generator(
        dataset_name, dataset_network_size, order, edges, dataset_sample_size, lifting_func, initialisation= initialisation)
    data = data_generator.data
    non_linear=True
    num_workers = 10
    num_epochs = 100
    num_samples=90
    data_dir = 'results/cross_validation/' + dataset_name +"/data_" + str(dataset_sample_size) + '_' + str(
        dataset_network_size) + '_' + str(order) + '_' + lifting_func.name() + '_' + initialisation

    for nonlinear_cut_model in [2,3,4]:

        config={'learning_rate': tune.loguniform(1e-7, 1e-1), 
                'regularization': tune.loguniform(1e-7, 1e-1), 
                'num_layers': tune.grid_search([3])}

        reporter = CLIReporter(
        parameter_columns=["num_layers",'learning_rate','regularization'],
        metric_columns=["loss"])

        scheduler = ASHAScheduler(
        max_t=num_epochs,
        grace_period=1,
        reduction_factor=2)

        train_fn_with_parameters = tune.with_parameters(crossvalidate_dataset, data=data, data_dir=data_dir,order=order, non_linear = non_linear, nonlinear_cut=nonlinear_cut_model, num_epochs=num_epochs, num_workers=num_workers, cv_fold=1, num_folds=3, hyperparameter_tuning=True)

        resources_per_trial = {"cpu": 1, "gpu": 1/num_workers}

        tuner = tune.Tuner(
            tune.with_resources(
                train_fn_with_parameters,
                resources=resources_per_trial
            ),
            tune_config=tune.TuneConfig(
                metric="loss",
                mode="min",
                scheduler=scheduler,
                num_samples=num_samples,
            ),
            run_config=air.RunConfig(
                name=data_dir+"_model_non_linear"+str(non_linear)+ '_'+str(nonlinear_cut_model),
                progress_reporter=reporter,
            ),
            param_space=config,
        )

        analysis = tuner.fit()  
            

        print(analysis.get_best_result(metric="loss", mode="min").config)

        total_cv_folds=10
        num_workers=3

        with Pool(num_workers) as p:
            p.starmap(crossvalidate_dataset, [(analysis.get_best_result(metric="loss", mode="min").config, data, data_dir,order, non_linear , nonlinear_cut_model, num_epochs, 0, i,total_cv_folds,False) for i in range(total_cv_folds)])


