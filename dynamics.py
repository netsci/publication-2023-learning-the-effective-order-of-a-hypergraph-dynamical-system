import numpy as np
import networkx as nx
import torch as pt
#import torch_geometric as ptg
import pytorch_lightning as ptl
import torch
from torch.nn import Sequential as Seq, Linear, ReLU
#from torch_geometric.nn import MessagePassing
from pytorch_lightning import LightningModule
from torch.optim.adam import Adam
from torch.optim import SparseAdam, Adagrad
from pytorch_lightning.loggers import TensorBoardLogger
#from torch_geometric.loader import DataLoader, DataListLoader
#from torch.utils.data import DataLoader
import warnings
import hypernetx
import collections
import sys
import ray.tune as tune
#from torch_geometric.data import InMemoryDataset, download_url
#from torch_geometric.data.collate import collate
warnings.filterwarnings('ignore', message="An output with one or more elements was resized since it had shape", category=UserWarning)




class LiftingFunction():
    def __init__(self) -> None:
        pass
    
    def func(self, input : pt.Tensor, order : int) -> pt.Tensor:
        raise(NotImplementedError)

    def apply(self, input : pt.Tensor, order) -> pt.Tensor:
        assert input.shape[1] == order
        output = self.func(input, order)
        assert output.shape[0] == input.shape[0]
        assert output.shape[1] == 1
        return output

    def name(self) -> str:
        raise(NotImplementedError)

class LinearLiftingFunction(LiftingFunction):
    def __init__(self, order, weights=[]) -> None:
        self.order = order
        if len(weights) > 0:
            self.weights = weights
        else:
            self.weights = [torch.rand(d, 1) for d in range(2, self.order+1)]

    def name(self):
        return 'linear'
    
    def func(self, input, order):
        return pt.matmul(input, self.weights[order - 2])

class CosLinearLiftingFunction(LiftingFunction):
    def __init__(self, order, weights=[]) -> None:
        self.order = order
        if len(weights) > 0:
            self.weights = weights
        else:
            self.weights = [torch.rand(d, 1) for d in range(2, self.order+1)]

    def name(self):
        return 'sum-of-cos'
    
    def func(self, input, order):
        return pt.matmul(pt.cos(input), self.weights[order - 2])

class LinearMultLiftingFunction(LiftingFunction):
    def __init__(self, order, nonlinear_cut=-1, weights=[]) -> None:
        self.order = order
        if nonlinear_cut == -1:
            self.nonlinear_cut = order
        else:
            self.nonlinear_cut = nonlinear_cut
        if len(weights) > 0:
            self.weights = weights
        else:
            self.weights = [torch.rand(d, 1) for d in range(2, self.order+1)]

    def name(self):
        return 'linear_mult'+str(self.nonlinear_cut)
    
    def func(self, input, order):
        return pt.prod(input[:,:self.nonlinear_cut],dim=1).reshape(-1, 1)

class LinearSinLiftingFunction(LiftingFunction):
    def __init__(self, order, nonlinear_cut=-1, weights=[]) -> None:
        self.order = order
        if nonlinear_cut == -1:
            self.nonlinear_cut = order
        else:
            self.nonlinear_cut = nonlinear_cut
        if len(weights) > 0:
            self.weights = weights
        else:
            self.weights = [torch.rand(d, 1) for d in range(2, self.order+1)]

    def name(self):
        return 'linear_sin'+str(self.nonlinear_cut)
    
    def func(self, input, order):
        return pt.sin(pt.sum(input[:,:self.nonlinear_cut], dim=1)).reshape(-1,1)

class KuramotoLiftingFunction(LiftingFunction):
    def __init__(self, order, nonlinear_cut=-1) -> None:
        self.order = order
        if nonlinear_cut == -1:
            self.nonlinear_cut = order
        else:
            self.nonlinear_cut = nonlinear_cut
        self.weights = [torch.cat((torch.tensor([[-(d-1) if d-1 < self.nonlinear_cut-1 else - (
            self.nonlinear_cut-1)]]), torch.ones(d-1, 1))) for d in range(2, self.order+1)]

    def name(self):
        return 'linear_kuramoto_'+str(self.nonlinear_cut)
    
    def func(self, input, order):
        return pt.sin(pt.sum(pt.matmul(input[:,:self.nonlinear_cut], self.weights[order - 2][:self.nonlinear_cut]), dim=1)).reshape(-1,1)


class DiffusionLiftingFunction(LiftingFunction):
    def __init__(self, order, nonlinear_cut=-1) -> None:
        self.order = order
        if nonlinear_cut == -1:
            self.nonlinear_cut = order
        else:
            self.nonlinear_cut = nonlinear_cut
        self.weights = [torch.cat((torch.tensor(
            [[-(d-1) if d-1 < self.nonlinear_cut-1 else - (self.nonlinear_cut-1)]]), torch.ones(d-1, 1))) for d in range(2, self.order+1)]

    def name(self):
        return 'diffusion_'+str(self.nonlinear_cut)

    def func(self, input, order):
        return pt.sum(pt.matmul(input[:, :self.nonlinear_cut], self.weights[order - 2][:self.nonlinear_cut]), dim=1).reshape(-1, 1)


class MCMILiftingFunction(LiftingFunction):
    def __init__(self, order, nonlinear_cut=-1) -> None:
        self.order = order
        if nonlinear_cut == -1:
            self.nonlinear_cut = order
        else:
            self.nonlinear_cut = nonlinear_cut
        self.weights = [torch.cat((torch.tensor(
            [[-(d-1) if d-1 < self.nonlinear_cut-1 else - (self.nonlinear_cut-1)]]), torch.ones(d-1, 1))) for d in range(2, self.order+1)]

    def name(self):
        return 'MCMI_'+str(self.nonlinear_cut)

    def func(self, input, order):
        influence_term = pt.exp(-10*pt.abs(pt.add(pt.mean(input[:, :self.nonlinear_cut], dim=1),(-1)*input[:,0])))
        return pt.mul(influence_term,pt.sum(pt.matmul(input[:, :self.nonlinear_cut], self.weights[order - 2][:self.nonlinear_cut]), dim=1)).reshape(-1, 1)

        


class ProductLiftingFunction(LiftingFunction):
    def __init__(self, order, weights=[]) -> None:
        self.order = order

    def name(self):
        return 'prod'
    
    def func(self, input, order):
        return pt.prod(input, dim=1).reshape(-1, 1)


class SILiftingFunction(LiftingFunction):
    def __init__(self, order, nonlinear_cut=-1, weights=[]) -> None:
        self.order = order
        if nonlinear_cut == -1:
            self.nonlinear_cut = order
        else:
            self.nonlinear_cut = nonlinear_cut
        if len(weights) > 0:
            self.weights = weights
        else:
            self.weights = [torch.rand(d, 1) for d in range(2, self.order+1)]

    def name(self):
        return 'SI_'+str(self.nonlinear_cut)

    def func(self, input, order):
        infec_prob = pt.prod(
            input[:, 1:self.nonlinear_cut], dim=1)
        output = pt.mul(
            pt.sub(1.0, input[:, 0]), infec_prob)
        return output.reshape(-1,1)
