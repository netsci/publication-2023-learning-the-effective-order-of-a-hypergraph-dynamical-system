import glob
import tensorflow as tf
import numpy as np






def return_single_result(non_linear, nonlinear_cut_model, agg, data_dir, num_folds):
    model_name = "model_non_linear" + \
        str(non_linear) + '_'+str(nonlinear_cut_model)
    cv_summary = []
    for path in [glob.glob(data_dir+"/"+model_name+'_fold'+str(i)+"/events*")[0] for i in range(num_folds)]:
        summary_val_loss = []
        for e in tf.compat.v1.train.summary_iterator(path):
            for v in e.summary.value:
                if v.tag == 'val_loss':
                    summary_val_loss.append(v.simple_value) 
        if agg == 'min':
            cv_summary.append(min(summary_val_loss))
        elif agg == 'last':
            cv_summary.append(summary_val_loss[-1])
        elif agg == 'min_last_10':
            cv_summary.append(min(summary_val_loss[-10:]))
        elif agg == 'mean_last_10':
            cv_summary.append(np.mean(summary_val_loss[-10:]))
    return np.mean(np.mean(np.array(cv_summary)[:]))




def bar_plot(ax, results, dataset_name, alpha):
    standard_colors = [([.12, .47, .71]), ([1., .49, .05]), ([.17, .62, .17])]
    colors = []
    for j in range(3):
        rgba_colors = np.zeros((3, 4))
        rgba_colors[:, 0] = standard_colors[j][0]
        rgba_colors[:, 1] = standard_colors[j][1]
        rgba_colors[:, 2] = standard_colors[j][2]
        rgba_colors[:, 3] = alpha[j]
        colors.append(rgba_colors)

    offset = [-0.15, 0, 0.15]
    x = np.arange(3)
    width = 0.15

    for k in range(3):
        ax.bar(x+offset[k], results[k], width, color=colors[k])
    ax.set_xlabel(r"$p$")
    ax.set_xticks([0, 1, 2], ['2', '3', '4'])
