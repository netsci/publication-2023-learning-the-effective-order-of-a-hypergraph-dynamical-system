import numpy as np
import networkx as nx
import torch as pt
import torch_geometric as ptg
import pytorch_lightning as ptl
import torch
from torch.nn import Sequential as Seq, Linear, ReLU
#from torch_geometric.nn import MessagePassing
from pytorch_lightning import LightningModule
from torch.optim.adam import Adam
from torch.optim import SparseAdam, Adagrad
from pytorch_lightning.loggers import TensorBoardLogger
from torch_geometric.loader import DataLoader, DataListLoader
#from torch.utils.data import DataLoader
import warnings
import hypernetx
import collections
import xgi
import sys
import ray.tune as tune
from torch_geometric.data import InMemoryDataset, download_url
from torch_geometric.data.collate import collate
from dynamics import LiftingFunction
import itertools
warnings.filterwarnings('ignore', message="An output with one or more elements was resized since it had shape", category=UserWarning)


def initialise_X(size_hypergraph, initialisation):
    if initialisation == "01uniform":
        X = pt.rand(size_hypergraph, 1)
    elif initialisation == "-11uniform":
        X = pt.Tensor(size_hypergraph, 1).uniform_(-1, 1)
    elif initialisation == "-pipiuniform":
        X = pt.Tensor(size_hypergraph, 1).uniform_(-np.pi, np.pi)
    elif initialisation == "-pipiuniform":
        X = pt.Tensor(size_hypergraph, 1).uniform_(-np.pi, np.pi)
    elif initialisation == "skewed":
        X = pt.Tensor(size_hypergraph, 1)
        a = np.random.uniform(0, 1)
        print(a)
        for i in range(size_hypergraph):
            b = np.random.uniform(0, 1)
            if b < a:
                X[i] = np.random.uniform(a, 1)
            else:
                X[i] = np.random.uniform(0, a)
    return X

def boundry_from_edge_list(hyper_edge_list, num_nodes, order=-1):
    if order == -1:
        max_order = np.max([len(x) for x in hyper_edge_list])
        L = [[] for i in range(2, max_order+1)]
    else:
        L = [[] for i in range(2, order+1)]
    
    for edge in hyper_edge_list:
        d = len(edge)
        for comb in itertools.permutations(edge, r=d):
            
            e = [[1 if comb[j] == i else 0 for i in range(num_nodes)] for j in range(len(comb))]
            #print(comb, e)
            L[d-2].extend(e)
    # for i, l in enumerate(L):
    #     if len(l) == 0:
    #         L[i] = [[0] * num_nodes] * (i+2)
    return L


class MyData(ptg.data.Data):
    def __inc__(self, key, value, *args, **kwargs):
        #print(key, value)
        if key == 'hyper_edge_index':
            #print(self.hyper_edge_index[0])
            #print(torch.tensor([[len(self.hyper_edge_index[0])], [len(self.x)]]))
            return torch.tensor([[len(value[0])], [len(self.x)]])
        else:
            return super().__inc__(key, value, *args, **kwargs)


class HashFunction:
    def __init__(self):
        self.reset()

    def apply(self, value):
        if value not in self.hash_dict:
            self.hash_dict[value] = self.hash_counter
            self.hash_counter += 1
        return self.hash_dict[value]

    def reset(self):
        self.hash_dict = {}
        self.hash_counter = 0


class simple_ERHypergraph_Dataset(InMemoryDataset):
    def __init__(self, root: str, size_hypergraph: int, order: int, num_samples: int, lifting_func: LiftingFunction, initialisation="01uniform",  transform=None, pre_transform=None, pre_filter=None):
        self.lifting_func = lifting_func
        self.size_hypergraph = size_hypergraph
        self.order = order
        self.num_samples = num_samples
        self.initialisation = initialisation
        super().__init__("data/"+root, transform, pre_transform, pre_filter)

        self.data = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        return []

    @property
    def processed_file_names(self):
        return ['data_' + str(self.num_samples) + '_' + str(self.size_hypergraph) + '_' + str(self.order) + '_' + self.lifting_func.name() + '_' + self.initialisation + '.pt']

    def download(self):
        pass
        

    def process(self):
        data_list = []
        weights = [torch.rand(d, 1) for d in range(2, self.order+1)]

        for sample in range(self.num_samples):
            #H = hypernetx.algorithms.generative_models.erdos_renyi_hypergraph(self.size_hypergraph,30,0.10)
            ps = [0.1, 0.01, 0.001]
            H = xgi.random_hypergraph(self.size_hypergraph, ps)
            h_dict = xgi.to_hyperedge_dict(H)
            edges = [[*h_dict[i]] for i in h_dict.keys() if len(h_dict[i]) > 1 and len(h_dict[i]) <= self.order]
            #edges = [[*H.incidence_dict[i]] for i in H.incidence_dict.keys() if len(H.incidence_dict[i]) > 1 and len(H.incidence_dict[i]) <= self.order]
            dense_L = boundry_from_edge_list(edges, self.size_hypergraph, order=self.order)
            #print(dense_L)
            L = []
            for i in range(len(dense_L)):
                L.append(pt.Tensor(dense_L[i]).to_sparse_coo())

            for i in range(len(dense_L)):
                L[i] = L[i].coalesce()
                dense_L[i] = L[i].indices()
            #print(dense_L)
            if self.initialisation == "01uniform":
                X = pt.rand(self.size_hypergraph, 1)
            elif self.initialisation == "-11uniform":
                X = pt.Tensor(self.size_hypergraph, 1).uniform_(-1, 1)
            elif self.initialisation == "-pipiuniform":
                X = pt.Tensor(self.size_hypergraph, 1).uniform_(-np.pi, np.pi)
            elif self.initialisation == "-pipiuniform":
                X = pt.Tensor(self.size_hypergraph, 1).uniform_(-np.pi, np.pi)
            elif self.initialisation == "skewed":
                X = pt.Tensor(self.size_hypergraph, 1)
                a = np.random.uniform(0,1)
                for i in range(self.size_hypergraph):
                    b=np.random.uniform(0,1)
                    if b<a:
                        X[i] = np.random.uniform(a, 1)
                    else:
                        X[i] = np.random.uniform(0, a)
            squeezed = pt.zeros((X.shape[0], 1))
            for d in range(len(L)):
                B = L[d]
                if len(B.values()) == 0:
                    continue
                
                lifted = pt.matmul(B, X).reshape(-1,d+2)

                out = self.lifting_func.apply(lifted, d+2)
                #out = pt.matmul(lifted, weights[d])
                
                    #out = pt.matmul(pt.cos(lifted), weights[d])
                    #out = pt.prod(lifted, dim=1).reshape(-1, 1)

                nodes_to_update = [(d+2)*i for i in range(int(B.shape[0]/(d+2)))]

                down = pt.sparse_coo_tensor(pt.Tensor([range(len(nodes_to_update)),B.indices()[1, nodes_to_update]]), [1.0]*len(nodes_to_update), size=(len(nodes_to_update), self.size_hypergraph))

                squeezed = pt.add(squeezed, pt.matmul(down.transpose(0,1), out))
            
            #print(dense_L)
            data_list.append(MyData(x = X, y=squeezed, hyper_edge_index=dense_L))

        

        #data, slices = self.collate(data_list)

        #print(data.hyper_edge_index)
        torch.save(data_list, self.processed_paths[0])


class simple_ERHypergraph_Trajectories(InMemoryDataset):
    def __init__(self, root : str, size_hypergraph : int, order :int , num_samples : int, lifting_func : LiftingFunction, number_time_steps : int, time_delta : int, initialisation="01uniform", transform=None, pre_transform=None, pre_filter=None):
        self.lifting_func = lifting_func
        self.size_hypergraph = size_hypergraph
        self.order = order
        self.num_samples = num_samples
        self.number_time_steps= number_time_steps
        self.time_delta = time_delta
        self.initialisation = initialisation
        super().__init__("data/"+root, transform, pre_transform, pre_filter)
        self.data = torch.load(self.processed_paths[0])
        

    @property
    def raw_file_names(self):
        return []

    @property
    def processed_file_names(self):
        return ['data_trajectories_' + str(self.num_samples) + '_' + str(self.size_hypergraph) + '_' + str(self.order) + '_' + self.lifting_func.name() + '_' + self.initialisation + "_" + str(self.number_time_steps) + "_" + str(self.time_delta) + '.pt']

    def download(self):
        pass
        

    def process(self):
        data_list = []

        for sample in range(self.num_samples):
            H = hypernetx.algorithms.generative_models.erdos_renyi_hypergraph(self.size_hypergraph,30,0.10)
            edges = [[*H.incidence_dict[i]] for i in H.incidence_dict.keys() if len(H.incidence_dict[i]) > 1 and len(H.incidence_dict[i]) <= self.order]
            dense_L = boundry_from_edge_list(edges, self.size_hypergraph, order=self.order)
            #print(dense_L)
            L = []
            for i in range(len(dense_L)):
                L.append(pt.Tensor(dense_L[i]).to_sparse_coo())

            for i in range(len(dense_L)):
                L[i] = L[i].coalesce()
                dense_L[i] = L[i].indices()
            #print(dense_L)
            if self.initialisation == "01uniform":
                X = pt.rand(self.size_hypergraph, 1)
            elif self.initialisation == "-11uniform":
                X = pt.Tensor(self.size_hypergraph, 1).uniform_(-1, 1)
            elif self.initialisation == "-pipiuniform":
                X = pt.Tensor(self.size_hypergraph, 1).uniform_(-np.pi, np.pi)
            elif self.initialisation == "-pipiuniform":
                X = pt.Tensor(self.size_hypergraph, 1).uniform_(-np.pi, np.pi)
            elif self.initialisation == "skewed":
                X = pt.Tensor(self.size_hypergraph, 1)
                a = np.random.uniform(0, 1)
                for i in range(self.size_hypergraph):
                    b = np.random.uniform(0, 1)
                    if b < a:
                        X[i] = np.random.uniform(a, 1)
                    else:
                        X[i] = np.random.uniform(0, a)
            curr=X
            time_series=[]
            for t in range(self.number_time_steps):
                squeezed = pt.zeros((X.shape[0], 1))
                for d in range(len(L)):
                    B = L[d]
                    if len(B.values()) == 0:
                        continue
                    
                    lifted = pt.matmul(B, curr).reshape(-1,d+2)

                    out = self.lifting_func.apply(lifted, d+2)
                    #out = pt.matmul(lifted, weights[d])
                    
                        #out = pt.matmul(pt.cos(lifted), weights[d])
                        #out = pt.prod(lifted, dim=1).reshape(-1, 1)

                    nodes_to_update = [(d+2)*i for i in range(int(B.shape[0]/(d+2)))]


                    down = pt.sparse_coo_tensor(pt.Tensor([range(len(nodes_to_update)),B.indices()[1, nodes_to_update]]), [1.0]*len(nodes_to_update), size=(len(nodes_to_update), self.size_hypergraph))

                    squeezed = pt.add(squeezed, pt.matmul(down.transpose(0,1), out))
                curr=pt.add(curr,pt.mul(self.time_delta,squeezed))
                time_series.append(curr)
                
            #print(dense_L)
            data_list.append(MyData(x = X, y=time_series, hyper_edge_index=dense_L))

        

        #data, slices = self.collate(data_list)

        #print(data.hyper_edge_index)
        torch.save(data_list, self.processed_paths[0])


class Hypergraph_Trajectory_Generator(InMemoryDataset):

    def __init__(self, root: str, size_hypergraph: int, order: int, edges: list, X: pt.Tensor, lifting_func: LiftingFunction, number_time_steps: int, time_delta: int, transform=None, pre_transform=None, pre_filter=None):
        self.lifting_func = lifting_func
        self.size_hypergraph = size_hypergraph
        self.order = order
        self.number_time_steps = number_time_steps
        self.time_delta = time_delta
        self.edges = edges
        self.X = X
        super().__init__("data/"+root, transform, pre_transform, pre_filter)
        self.data = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        return []

    @property
    def processed_file_names(self):
        return ['data_trajectories_' + str(self.size_hypergraph) + '_' + str(self.order) + '_' + self.lifting_func.name() + "_" + str(self.number_time_steps) + "_" + str(self.time_delta) + '.pt']

    def download(self):
        pass

    def process(self):

        dense_L = boundry_from_edge_list(
                self.edges, self.size_hypergraph, order=self.order)
            #print(dense_L)
        L = []
        for i in range(len(dense_L)):
            L.append(pt.Tensor(dense_L[i]).to_sparse_coo())

        for i in range(len(dense_L)):
            L[i] = L[i].coalesce()
            dense_L[i] = L[i].indices()
            #print(dense_L)
        curr = self.X
        time_series = []
        for t in range(self.number_time_steps):
            squeezed = pt.zeros((self.X.shape[0], 1))
            for d in range(len(L)):
                B = L[d]
                if len(B.values()) == 0:
                    continue

                lifted = pt.matmul(B, curr).reshape(-1, d+2)

                out = self.lifting_func.apply(lifted, d+2)
                    #out = pt.matmul(lifted, weights[d])

                    #out = pt.matmul(pt.cos(lifted), weights[d])
                    #out = pt.prod(lifted, dim=1).reshape(-1, 1)

                nodes_to_update = [
                        (d+2)*i for i in range(int(B.shape[0]/(d+2)))]

                down = pt.sparse_coo_tensor(pt.Tensor([range(len(nodes_to_update)), B.indices()[1, nodes_to_update]]), [
                                                1.0]*len(nodes_to_update), size=(len(nodes_to_update), self.size_hypergraph))

                squeezed = pt.add(squeezed, pt.matmul(
                        down.transpose(0, 1), out))
            curr = pt.add(curr, pt.mul(self.time_delta, squeezed))
            time_series.append(curr)

            #print(dense_L)

            data=MyData(x=self.X, y=time_series, hyper_edge_index=dense_L)

        #data, slices = self.collate(data_list)

        #print(data.hyper_edge_index)
        torch.save(data, self.processed_paths[0])


class Hypergraph_Generator(InMemoryDataset):

    def __init__(self, root: str, size_hypergraph: int, order: int, edges:list, num_samples: int, lifting_func: LiftingFunction, initialisation="01uniform",  transform=None, pre_transform=None, pre_filter=None):
        self.lifting_func = lifting_func
        self.size_hypergraph = size_hypergraph
        self.order = order
        self.edges = edges
        self.num_samples = num_samples
        self.initialisation = initialisation
        super().__init__("data/"+root, transform, pre_transform, pre_filter)
        self.data = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        return []

    @property
    def processed_file_names(self):
        return ['data_' + str(self.num_samples) + '_' + str(self.size_hypergraph) + '_' + str(self.order) + '_' + self.lifting_func.name() + '_' + self.initialisation + '.pt']

    def download(self):
        pass

    def process(self):
        data_list = []

        for sample in range(self.num_samples):
            dense_L = boundry_from_edge_list(
                self.edges, self.size_hypergraph, order=self.order)
            L = []
            for i in range(len(dense_L)):
                L.append(pt.Tensor(dense_L[i]).to_sparse_coo())

            for i in range(len(dense_L)):
                L[i] = L[i].coalesce()
                dense_L[i] = L[i].indices()
            if self.initialisation == "01uniform":
                X = pt.rand(self.size_hypergraph, 1)
            elif self.initialisation == "-11uniform":
                X = pt.Tensor(self.size_hypergraph, 1).uniform_(-1, 1)
            elif self.initialisation == "-pipiuniform":
                X = pt.Tensor(self.size_hypergraph, 1).uniform_(-np.pi, np.pi)
            elif self.initialisation == "-pipiuniform":
                X = pt.Tensor(self.size_hypergraph, 1).uniform_(-np.pi, np.pi)
            elif self.initialisation == "skewed":
                X = pt.Tensor(self.size_hypergraph, 1)
                a = np.random.uniform(0, 1)
                for i in range(self.size_hypergraph):
                    b = np.random.uniform(0, 1)
                    if b < a:
                        X[i] = np.random.uniform(a, 1)
                    else:
                        X[i] = np.random.uniform(0, a)
            squeezed = pt.zeros((X.shape[0], 1))
            for d in range(len(L)):
                B = L[d]
                if len(B.values()) == 0:
                    continue

                lifted = pt.matmul(B, X).reshape(-1, d+2)

                out = self.lifting_func.apply(lifted, d+2)
                #out = pt.matmul(lifted, weights[d])

                #out = pt.matmul(pt.cos(lifted), weights[d])
                #out = pt.prod(lifted, dim=1).reshape(-1, 1)

                nodes_to_update = [
                    (d+2)*i for i in range(int(B.shape[0]/(d+2)))]

                down = pt.sparse_coo_tensor(pt.Tensor([range(len(nodes_to_update)), B.indices()[1, nodes_to_update]]), [
                                            1.0]*len(nodes_to_update), size=(len(nodes_to_update), self.size_hypergraph))

                squeezed = pt.add(squeezed, pt.matmul(
                    down.transpose(0, 1), out))

            #print(dense_L)
            data_list.append(MyData(x=X, y=squeezed, hyper_edge_index=dense_L))
        torch.save(data_list, self.processed_paths[0])
