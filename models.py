import numpy as np
import networkx as nx
import torch as pt
#import torch_geometric as ptg
import pytorch_lightning as ptl
import torch
from torch.nn import Sequential as Seq, Linear, ReLU
#from torch_geometric.nn import MessagePassing
from pytorch_lightning import LightningModule
from torch.optim.adam import Adam
from torch.optim import SparseAdam, Adagrad
from pytorch_lightning.loggers import TensorBoardLogger
#from torch_geometric.loader import DataLoader, DataListLoader
#from torch.utils.data import DataLoader
import warnings
import hypernetx
import collections
import sys
import os
import ray.tune as tune
warnings.filterwarnings('ignore', message="An output with one or more elements was resized since it had shape", category=UserWarning)



def create_layers(num_layers,i=0,nonlinear_cut=0):
        layers_dict = collections.OrderedDict()
        size_mlp=2**num_layers*16
        layers_dict.update({'linear_encoding' : torch.nn.Linear(min(i, nonlinear_cut), size_mlp)})
        for i in range(num_layers-1):
                layers_dict.update({'relu'+str(i) : torch.nn.ReLU()})
                layers_dict.update({'linear'+str(i) : torch.nn.Linear(size_mlp, size_mlp)})
        layers_dict.update({'relu_out' : ReLU()})
        layers_dict.update({'linear_out' : torch.nn.Linear(size_mlp, 1)})
        return torch.nn.Sequential(layers_dict)


class HigherOrderModel(LightningModule):
    def __init__(self, config, order=0, nonlinear_cut=0):
        super().__init__()
        self.learning_rate = config['learning_rate']
        self.regularization = config['regularization']
        self.num_layers = config['num_layers']
        self.loss_func = pt.nn.L1Loss()
        self.order = order
        self.nonlinear_cut = nonlinear_cut
        self.models = torch.nn.ModuleList([create_layers(self.num_layers,i,self.nonlinear_cut) for i in range(2, self.order +1)])
        self.weights = torch.nn.ParameterList([torch.nn.Parameter(pt.Tensor(pt.rand((min(i, self.nonlinear_cut), 1)))) for i in range(2, self.order +1)])
        self.save_hyperparameters()
    def forward(self, x, hyper_edge_index):
        # x has shape [N, in_channels]
        # edge_index has shape [2, E]
        squeezed = pt.zeros_like(x)
        for d in range(self.order - 1):
            if len(hyper_edge_index[d][0]) == 0:
                continue
            L = pt.sparse_coo_tensor(pt.Tensor(hyper_edge_index[d]), [1.0]*len(hyper_edge_index[d][0]), device=self.device, size=(len(hyper_edge_index[d][0]), x.shape[0]))
            L = L.coalesce()
            lifted = pt.matmul(L, x)
            lifted = lifted.reshape(-1,d+2)
            out = self.models[d](lifted[:,:self.nonlinear_cut])

            nodes_to_update = [(d+2)*i for i in range(int(L.shape[0]/(d+2)))]
            down = pt.sparse_coo_tensor(pt.Tensor([range(len(nodes_to_update)),L.indices()[1, nodes_to_update]]), [1.0]*len(nodes_to_update), device=self.device, size=(len(nodes_to_update), x.shape[0]))
            squeezed = pt.add(squeezed, pt.matmul(down.transpose(0,1), out))
        
        return squeezed # x + squeezed

    def training_step(self, batch, batch_idx):
        # Forward pass
        prediction = self(batch.x, batch.hyper_edge_index)

        # Compute loss
        loss = self.loss_func(prediction, batch.y)

        # Logging
        #self.train_acc(self.activation_func(prediction), batch.y)
        self.log('train_loss', loss, on_epoch=True, prog_bar=True)
            
        #self.log('penalty', loss_penalty, on_epoch=True, prog_bar=True)
        #self.log('penalty_grad', loss_penalty.requires_grad, on_epoch=True, prog_bar=True)
        #print(loss_penalty.requires_grad)

        # Loss is passed to backward pass
        return loss

    def validation_step(self, batch, batch_idx):
        # Forward pass
        prediction = self(batch.x, batch.hyper_edge_index)
        loss = self.loss_func(prediction, batch.y)

        # Logging
        #self.val_acc(self.activation_func(prediction), batch.y)
        #self.val_prec(self.activation_func(prediction), batch.y)
        self.log('val_loss', loss)
        #self.log('val_prec', self.val_prec, on_epoch=True, prog_bar=True)
        #self.log('val_acc', self.val_acc, on_epoch=True, prog_bar=True)


    def configure_optimizers(self):
        return Adam(self.parameters(), lr=self.learning_rate, weight_decay=self.regularization)