# Publication 2023 - Learning the effective order of a hypergraph dynamical system

## **This Repo is still under construction! Not all features are currently available**

In this repository, you can find the code used in the paper "Learning the effective order of a hypergraph dynamical system". You can find the paper [here](https://arxiv.org/abs/2306.01813).

### Installation
All necessary packages can be installed using: 
`pip install -r requirements.txt`
However, this will install the cpu version of PyTorch and PyTorch Geometric. If you wish to use your GPU, you should follow the installation process [here](https://pytorch.org/get-started/locally/) for PyTorch first and then install PyTorch Geometric [here](https://pytorch-geometric.readthedocs.io/en/latest/install/installation.html) and then the remaining packages.

### Figures 
The code needed to reproduce the figures in the paper can be found in the various notebooks.

### Launching your own trials
*This is still work in progress, this repo does not have full functionality yet.*
The main functionality is written within the cross_validation.py file. Calling: 

`python cross_validation.py --args` 

will start a trial whose output will be stored in /results. You can see what options you can add by calling:

`python cross_validation.py -h`.

A sample call on the highschool using kuramoto dynamics could look like this:

`python cross_validation.py -ds data/highschool.npy - dy Kuramoto`.

