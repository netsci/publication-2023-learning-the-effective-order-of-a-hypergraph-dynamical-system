import numpy as np
import networkx as nx
import torch as pt
import pytorch_lightning as ptl
import torch_geometric as ptg
import torch
from torch.nn import Sequential as Seq, Linear, ReLU
from torch_geometric.nn import MessagePassing
from pytorch_lightning import LightningModule
from torch.optim.adam import Adam
from torch.optim import SparseAdam, Adagrad
from pytorch_lightning.loggers import TensorBoardLogger
from torch.multiprocessing import Pool,  set_start_method
from torch_geometric.loader import DataLoader, DataListLoader
import warnings
import hypernetx
import collections
import sys
import ray.tune as tune
import ray.air as air
from ray.train import get_context
from ray.tune.integration.pytorch_lightning import TuneReportCallback
from ray.tune.schedulers import ASHAScheduler, FIFOScheduler
from ray.tune import CLIReporter
from models import HigherOrderModel
from hypergraphs import simple_ERHypergraph_Dataset,MyData, Hypergraph_Generator, Hypergraph_Trajectory_Generator, simple_ERHypergraph_Trajectories
import dynamics
import os
import argparse
warnings.filterwarnings('ignore', message="An output with one or more elements was resized since it had shape", category=UserWarning)


set_start_method('spawn', True)
RAY_CHDIR_TO_TRIAL_DIR=0
TUNE_DISABLE_AUTO_CALLBACK_LOGGERS=1

# #cross_validation function

def crossvalidate_dataset(config, data=None, data_dir=None,  order=None, nonlinear_cut=0, num_epochs = 100,num_workers=32, num_folds=3, hyperparameter_tuning=False, gpu=False):
    model = HigherOrderModel(config,order, nonlinear_cut=nonlinear_cut)

    cv_fold = config['cv_fold']
    # Cut the data set into the folds for cross validation
    lower_bound = int(cv_fold/num_folds*len(data)) 
    upper_bound = int((cv_fold+1)/num_folds*len(data))
    train_dataset = data[:lower_bound]
    train_dataset.extend(data[upper_bound:])
    test_dataset = data[lower_bound:upper_bound]

    model_name = "model_non_linear"+ 'True_'+str(nonlinear_cut)+'_fold'+str(cv_fold)

    save_dir= get_context().get_trial_dir() + '/../../../' + model_name

    print('Here.')
    print(save_dir)
    #data = [ptg.data.Data(x = X[:,i].reshape(-1, 1), y=Y[:,i].reshape(-1,1), edge_index=L) for i in range(X.shape[1])]
    train_loader = DataLoader(train_dataset, batch_size=20, shuffle=True, pin_memory=True, num_workers=num_workers)
    val_loader = DataLoader(test_dataset, batch_size=20, pin_memory=True, num_workers=num_workers)

    callbacks = [TuneReportCallback({"loss": "val_loss"},on="validation_end")] #if hyperparameter_tuning else [TuneReportCallback({"loss": "val_loss"},on="validation_end"), TensorBoardLogger(save_dir, name=model_name, version=".")]
    #early_stopping = ptl.callbacks.EarlyStopping('val_loss', stopping_threshold=0.01, patience=50)
    trainer = ptl.Trainer(accelerator='gpu' if gpu else 'cpu', devices=1, max_epochs=num_epochs, enable_progress_bar=False, default_root_dir=save_dir, logger=TensorBoardLogger(
            save_dir=save_dir, name=None), callbacks=callbacks)
    trainer.fit(model, train_loader, val_loader)


def trial_str_creator(trial):
    print(trial.trial_id)
    return "0"



if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='crossvalidation', 
                                     description='Cross validation for hypergraph neural network')
    parser.add_argument('-o','--order', default=2, type=int, help='Order of the hypergraph neural network')
    parser.add_argument('-edges', '--hyperedge_list_file', type=str, default="data/test.npy", help='File containing the hyperedge list as a list of lists')
    parser.add_argument('-nsize','--dataset_network_size', type=int, default=20, help='Number of nodes in the hypergraph')
    parser.add_argument('-ds', '--dataset_sample_size', type=int, default=20, help='Number of samples in the dataset')
    parser.add_argument('-i', '--initialisation', type=str, default='01uniform', help='Initialisation of the hypergraph neural network. Can be one of the following: [01uniform], [-11uniform], [-pipiuniform], [skewed].', choices=['01uniform', '-11uniform', '-pipiuniform', 'skewed'])
    parser.add_argument('-dy', '--dynamics', type=str, default='Linear', help='Output directory for the results.', choices=['Linear','CosLinear', 'LinearMult', 'LinearSin', 'Kuramoto', 'Diffusion', 'MCMI', 'Product', 'SI'])
    parser.add_argument('-t', '--trajectory', type=bool, default=False, help='Determines if the dataset is based on trajectories or random samples.')
    parser.add_argument('-nw', '--num_workers', type=int, default=32, help='Number of workers (cpus).')
    parser.add_argument('-ne', '--num_epochs', type=int, default=10, help='Number of epochs.')
    parser.add_argument('-nsamples', '--num_samples', type=int, default=2, help='Number of distinct initialisations.')
    parser.add_argument('-cvf', '--num_folds', type=int, default=3, help='Number of the cross validation fold.')
    parser.add_argument('-gpu', '--gpu', type=bool, default=False, help='Number of gpus.')
    
    args = parser.parse_args()
    
    if args.hyperedge_list_file is not None:
        graph = np.load(args.hyperedge_list_file, allow_pickle=True).item()
        order = np.max([len(x) for x in graph['edge_list']])
    else:
        order = args.order
    
    if args.dynamics == 'Linear':
        lifting_func = dynamics.LinearLiftingFunction(order)
    elif args.dynamics == 'CosLinear':
        lifting_func = dynamics.CosLinearLiftingFunction(order)
    elif args.dynamics == 'LinearMult':
        lifting_func = dynamics.LinearMultLiftingFunction(order)
    elif args.dynamics == 'LinearSin':
        lifting_func = dynamics.LinearSinLiftingFunction(order)
    elif args.dynamics == 'Kuramoto':
        lifting_func = dynamics.KuramotoLiftingFunction(order)
    elif args.dynamics == 'Diffusion':
        lifting_func = dynamics.DiffusionLiftingFunction(order)
    elif args.dynamics == 'MCMI':
        lifting_func = dynamics.MCMILiftingFunction(order)
    elif args.dynamics == 'Product':   
        lifting_func = dynamics.ProductLiftingFunction(order)
    elif args.dynamics == 'SI':
        lifting_func = dynamics.SILiftingFunction(order)
    
    
    if args.trajectory:
        dataset_name = 'TrajectoryHypergraph'
        #data_generator = simple_ERHypergraph_Dataset(dataset_name, args.dataset_network_size, order, args.dataset_sample_size, lifting_func, initialisation=args.initialisation, trajectory=True)
        #data = data_generator.data
    else:
        if args.hyperedge_list_file is None:
            dataset_name = 'ERHypergraph'
            data_generator = simple_ERHypergraph_Dataset(dataset_name, args.dataset_network_size, order, args.dataset_sample_size, lifting_func, initialisation=args.initialisation)
            data = data_generator._data
        else:
            dataset_name = args.hyperedge_list_file.split('/')[-1].split('.')[0]
            graph = np.load(args.hyperedge_list_file, allow_pickle=True).item()
            data_generator = Hypergraph_Generator(dataset_name, graph['num_nodes'], order, graph['edge_list'], args.dataset_sample_size, lifting_func, initialisation=args.initialisation)
            data = data_generator._data

    print(data)
    #data_dir = 'results/cross_validation/' + dataset_name +"/data_" + str(args.dataset_sample_size) + '_' + str(args.dataset_network_size) + '_' + str(order) + '_' + lifting_func.name() + '_' + args.initialisation +'/trials'

    for nonlinear_cut_data in range(2,5):
        for nonlinear_cut_model in range(2,5):
            data_dir = os.getcwd() +'/results/tune/' + dataset_name +"/data_" + str(args.dataset_sample_size) + '_' + str(args.dataset_network_size) + '_' + str(order) + '_' + lifting_func.name() + '_'+ str(nonlinear_cut_data) + '_' + args.initialisation +'/trials'
            print(data_dir)
            
            
            config={'learning_rate': tune.loguniform(1e-7, 1e-1), 
                    'regularization': tune.loguniform(1e-7, 1e-1), 
                    'num_layers': tune.choice([1,2,3]),
                    'cv_fold': tune.choice([1])}

            reporter = CLIReporter(
            parameter_columns=["num_layers",'learning_rate','regularization'],
            metric_columns=["loss", "training_iteration"])

            scheduler = ASHAScheduler(  max_t=args.num_epochs,
                                        grace_period=1,
                                        reduction_factor=2)

            train_fn_with_parameters = tune.with_parameters(crossvalidate_dataset, data=data, data_dir=data_dir,order=order, nonlinear_cut=nonlinear_cut_model, num_epochs=args.num_epochs, num_workers=args.num_workers, num_folds=args.num_folds,hyperparameter_tuning=True, gpu=args.gpu)

            resources_per_trial = {"cpu": 1, "gpu": 1/args.num_workers if args.gpu else 0}
            tune_config = tune.TuneConfig(
                    metric="loss",
                    mode="min",
                    scheduler=scheduler,
                    num_samples=args.num_samples,
                    #trial_name_creator=trial_str_creator,
                    #trial_dirname_creator=trial_str_creator,
                )
            run_config = air.RunConfig(
                    local_dir=data_dir,
                    #name="",
                    progress_reporter=reporter,
                    verbose=1
                )
            
            tuner = tune.Tuner(
                tune.with_resources(
                    train_fn_with_parameters,
                    resources=resources_per_trial
                ),
                tune_config=tune_config,
                run_config=run_config,
                param_space=config,
            )
            #Tuner = tune.Tuner(tune.with_parameters(crossvalidate_dataset, data=data, dataset_name=dataset_name, order=order, non_linear = non_linear, nonlinear_cut=nonlinear_cut, num_epochs=100, cv_fold=1, num_folds=3))
            analysis = tuner.fit()  

            #analysis=tune.run(train_fn_with_parameters, 
            #                  metric='val_loss', mode='max', config=config, local_dir='tune_hyperparameter_search', name='test', resources_per_trial={'gpu':1/num_workers}, num_samples=1, verbose=1, progress_reporter=tune.CLIReporter(print_intermediate_tables=True,metric_columns=["val_loss"]))

            

            
            config = analysis.get_best_result(metric="loss", mode="min").config
            print(config)
            config['cv_fold'] = tune.grid_search([i for i in range(args.num_folds)])

            data_dir = os.getcwd() +'/results/cross_validation/' + dataset_name +"/data_" + str(args.dataset_sample_size) + '_' + str(args.dataset_network_size) + '_' + str(order) + '_' + lifting_func.name() + '_'+ str(nonlinear_cut_data) + '_' + args.initialisation + '/trials'
            
            reporter = CLIReporter(
            parameter_columns=[],
            metric_columns=["loss"])

            scheduler = FIFOScheduler()

            train_fn_with_parameters = tune.with_parameters(crossvalidate_dataset, data=data, data_dir=data_dir,order=order, nonlinear_cut=nonlinear_cut_model, num_epochs=args.num_epochs, num_workers=args.num_workers, num_folds=args.num_folds,hyperparameter_tuning=False, gpu=args.gpu)

            resources_per_trial = {"cpu": 1, "gpu": 1/args.num_workers if args.gpu else 0}

            cv_tuner = tune.Tuner(
                tune.with_resources(
                    train_fn_with_parameters,
                    resources=resources_per_trial
                ),
                tune_config=tune.TuneConfig(
                    metric="loss",
                    mode="min",
                    scheduler=scheduler,
                    num_samples=1,
                ),
                run_config=air.RunConfig(
                    local_dir=data_dir,
                    #name="model_non_linear"+str(nonlinear_cut_model),
                    
                    progress_reporter=reporter,
                    verbose=1
                ),
                param_space=config,
                #trial_str_creator=trial_str_creator,
            )
            #Tuner = tune.Tuner(tune.with_parameters(crossvalidate_dataset, data=data, dataset_name=dataset_name, order=order, non_linear = non_linear, nonlinear_cut=nonlinear_cut, num_epochs=100, cv_fold=1, num_folds=3))
            analysis = cv_tuner.fit()  
            
            
            
            # with Pool(args.num_workers) as p:
            #     p.starmap(crossvalidate_dataset, [(analysis.get_best_result(metric="loss", mode="min").config, data, data_dir,order , nonlinear_cut_model, args.num_epochs, 0, i,10,False) for i in range(10)])
                    
            print("Finished.")
        # config={'learning_rate': tune.loguniform(1e-7, 1e-1), 
                    #         'regularization': tune.loguniform(1e-7, 1e-1), 
                    #         'num_layers': tune.grid_search([1,2,3])}



# dataset_name = 'ERHypergraphXGI'
# order = 4
# dataset_network_size = 20
# dataset_sample_size = 500
# initialisation = "01uniform"

# for nonlinear_cut_data in range(2,order+1):
#     lifting_func=dynamics.SILiftingFunction_new(order, nonlinear_cut_data)
#     data_generator = simple_ERHypergraph_Dataset(
#         dataset_name, dataset_network_size, order, dataset_sample_size, lifting_func, initialisation= initialisation)
#     data = data_generator.data
#     non_linear=True
#     num_workers = 10
#     num_epochs = 100
#     num_samples=10
#     data_dir = 'results/cross_validation/' + dataset_name +"/data_" + str(dataset_sample_size) + '_' + str(
#         dataset_network_size) + '_' + str(order) + '_' + lifting_func.name() + '_' + initialisation

#     for nonlinear_cut_model in range(2,order+1):

#         config={'learning_rate': tune.loguniform(1e-7, 1e-1), 
#                 'regularization': tune.loguniform(1e-7, 1e-1), 
#                 'num_layers': tune.grid_search([1,2,3])}

#         reporter = CLIReporter(
#         parameter_columns=["num_layers",'learning_rate','regularization'],
#         metric_columns=["loss"])

#         scheduler = ASHAScheduler(
#         max_t=num_epochs,
#         grace_period=1,
#         reduction_factor=2)

#         train_fn_with_parameters = tune.with_parameters(crossvalidate_dataset, data=data, data_dir=data_dir,order=order, non_linear = non_linear, nonlinear_cut=nonlinear_cut_model, num_epochs=num_epochs, num_workers=num_workers, cv_fold=1, num_folds=3,hyperparameter_tuning=True)

#         resources_per_trial = {"cpu": 1, "gpu": 1/num_workers}

#         tuner = tune.Tuner(
#             tune.with_resources(
#                 train_fn_with_parameters,
#                 resources=resources_per_trial
#             ),
#             tune_config=tune.TuneConfig(
#                 metric="loss",
#                 mode="min",
#                 scheduler=scheduler,
#                 num_samples=num_samples,
#             ),
#             run_config=air.RunConfig(
#                 name=data_dir+"_model_non_linear"+str(non_linear)+ '_'+str(nonlinear_cut_model),
#                 progress_reporter=reporter,
#             ),
#             param_space=config,
#         )
#         #Tuner = tune.Tuner(tune.with_parameters(crossvalidate_dataset, data=data, dataset_name=dataset_name, order=order, non_linear = non_linear, nonlinear_cut=nonlinear_cut, num_epochs=100, cv_fold=1, num_folds=3))
#         analysis = tuner.fit()  
            
#         #analysis=tune.run(train_fn_with_parameters, 
#         #                  metric='val_loss', mode='max', config=config, local_dir='tune_hyperparameter_search', name='test', resources_per_trial={'gpu':1/num_workers}, num_samples=1, verbose=1, progress_reporter=tune.CLIReporter(print_intermediate_tables=True,metric_columns=["val_loss"]))

#         print(analysis.get_best_result(metric="loss", mode="min").config)

#         with Pool(num_workers) as p:
#             p.starmap(crossvalidate_dataset, [(analysis.get_best_result(metric="loss", mode="min").config, data, data_dir,order, non_linear , nonlinear_cut_model, num_epochs, 0, i,10,False) for i in range(10)])


